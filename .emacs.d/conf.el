;; Set up package.el to work with MELPA
(require 'package)
(add-to-list 'package-archives
             '("melpa" . "https://melpa.org/packages/"))
(package-initialize)
(unless package-archive-contents
  (package-refresh-contents))
(unless (package-installed-p 'use-package)
  (package-install 'use-package))

;;Terminal
(use-package org :ensure t)
(use-package org-evil :ensure t)

;; Download Evil
(unless (package-installed-p 'evil)
  (package-install 'evil))

;; Enable Evil
(require 'evil)
(evil-mode 1)
	
;;which key
(use-package which-key
	:ensure t
  :config
	(which-key-mode))
	
;;aspect
(use-package jetbrains-darcula-theme
	:ensure t
  :config
  (load-theme 'jetbrains-darcula t))
;;(set-frame-parameter (selected-frame) 'alpha '(85 . 50))
;;(add-to-list 'default-frame-alist '(alpha . (85 . 50)))
(menu-bar-mode -1)
(tool-bar-mode -1)
(add-to-list 'default-frame-alist '(fullscreen . maximized)) 
;;Config
(setq-default tab-width 2)

(global-display-line-numbers-mode 1)
(setq display-line-numbers-type 'relative)

;;Terminal
(use-package vterm
    :ensure t)

;;update
(use-package auto-package-update
	:ensure t
  :custom
  (auto-package-update-interval 7)
  (auto-package-update-prompt-before-update t)
;;  (auto-package-update-hide-results t)
  :config
  (auto-package-update-maybe)
;;(auto-package-update-at-time "09:00"))
)

;;programming
(use-package rustic :ensure t)

(use-package lsp-mode
  :ensure t 
	:init
	; set prefix for lsp-command-keymap (few alternatives - "C-l", "C-c l")
  (setq lsp-keymap-prefix "C-l")
  :hook (;; replace XXX-mode with concrete major-mode(e. g. python-mode)
         (XXX-mode . lsp)
         ;; if you want which-key integration
         (lsp-mode . lsp-enable-which-key-integration))
  :commands lsp)

(use-package lsp-ui  :ensure t :commands lsp-ui-mode)
(use-package helm-lsp :ensure t :commands helm-lsp-workspace-symbol)
(use-package lsp-ivy  :ensure t :commands lsp-ivy-workspace-symbol)
(use-package lsp-treemacs  :ensure t :commands lsp-treemacs-errors-list)
(use-package dap-mode
	:ensure t
	:config
	  ;;c/cpp/rust
	  (require 'dap-cpptools)
		(unless (file-directory-p "~/.emacs.d/.extension/vscode/cpptools") (dap-cpptools-setup))
)

